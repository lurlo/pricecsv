# pricecsv

Easily calculate the total of all products in csv

## TODO

- [ ] Add example
- [ ] Add usage guide
- [ ] Add optional CSV print mode
- [ ] Add possibility to append the `subtotal` and `total` in the csv

## License

GPL-3
